package com.sexhack.sexharassment.view

interface PostNotifCallback<T> {
    fun onPostNotif(data: T)
    fun onPostNotifError(message: String)
}
