package com.sexhack.sexharassment.view

interface GetNotifCallback<T> {
    fun onGetNotif(data: T)
    fun onGetNotifError(message: String)
}
