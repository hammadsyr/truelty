package com.sexhack.sexharassment.view

import com.sexhack.sexharassment.model.PostReport

interface PostReportView : PostReportCallback<PostReport> {
    fun onShowLoading()
}