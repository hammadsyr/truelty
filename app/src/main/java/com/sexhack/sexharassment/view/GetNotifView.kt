package com.sexhack.sexharassment.view

import com.sexhack.sexharassment.model.GetNotification

interface GetNotifView : GetNotifCallback<List<GetNotification>> {
    fun onShowLoading()
}