package com.sexhack.sexharassment.view

import com.sexhack.sexharassment.model.PostNotification

interface PostNotifView : PostNotifCallback<PostNotification> {
    fun onShowLoading()
}