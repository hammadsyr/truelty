package com.sexhack.sexharassment.view

import com.sexhack.sexharassment.model.CheckText

interface PostCheckView : PostCheckCallback<CheckText> {
    fun onShowLoading()
}