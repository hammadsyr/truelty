package com.sexhack.sexharassment.view

interface PostReportCallback<T> {
    fun onPostReport(data: T)
    fun onPostReportError(message: String)
}