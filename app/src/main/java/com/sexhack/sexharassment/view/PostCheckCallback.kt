package com.sexhack.sexharassment.view

interface PostCheckCallback<T> {
    fun onPostCheck(data: T)
    fun onPostCheckError(message: String)
}