package com.sexhack.sexharassment.util

class API {
    companion object {
        const val CREATE_NOTIF = "notif/create"
        const val LIST_NOTIF = "notif/all"
        const val CREATE_REPORT = "report/create"
        const val CHECK = "check"
    }
}