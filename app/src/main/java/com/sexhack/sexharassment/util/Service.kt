package com.sexhack.sexharassment.util

import com.google.gson.JsonObject
import com.sexhack.sexharassment.model.CheckText
import com.sexhack.sexharassment.model.GetNotification
import com.sexhack.sexharassment.model.PostNotification
import com.sexhack.sexharassment.model.PostReport
import io.reactivex.Observable
import okhttp3.MultipartBody
import retrofit2.http.*

interface Service {
    @GET(API.LIST_NOTIF)
    fun getNotif(): Observable<List<GetNotification>>

    @Headers("Content-Type: application/json")
    @POST(API.CREATE_NOTIF)
    fun postNotif(@Body body: JsonObject): Observable<PostNotification>

    @Headers("Content-Type: application/json")
    @POST(API.CHECK)
    fun postCheck(@Body body: JsonObject): Observable<CheckText>

    @Multipart
    @POST(API.CREATE_REPORT)
    fun postReport(@Part messageFrom: MultipartBody.Part, @Part firstFile: MultipartBody.Part?, @Part secondFile: MultipartBody.Part?,
                   @Part thirdFile: MultipartBody.Part?): Observable<PostReport>

}