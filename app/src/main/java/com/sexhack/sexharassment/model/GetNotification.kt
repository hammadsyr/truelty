package com.sexhack.sexharassment.model

data class GetNotification(
    val created_at: String,
    val from: String,
    val is_harassment : Boolean,
    val id: Int,
    val message: String,
    val updated_at: String
)