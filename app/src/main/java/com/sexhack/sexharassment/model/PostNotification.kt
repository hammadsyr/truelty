package com.sexhack.sexharassment.model

data class PostNotification(
    val response: String
)