package com.sexhack.sexharassment.model

data class PostReport(
    val created_at: String,
    val id: Int,
    val message_from: String,
    val screenshot_one: ScreenshotOne,
    val screenshot_three: ScreenshotThree,
    val screenshot_two: ScreenshotTwo,
    val updated_at: String
)

data class ScreenshotOne(
    val url: Any
)

data class ScreenshotTwo(
    val url: Any
)

data class ScreenshotThree(
    val url: Any
)