package com.sexhack.sexharassment

import android.inputmethodservice.InputMethodService
import android.inputmethodservice.Keyboard
import android.inputmethodservice.KeyboardView
import android.text.TextUtils
import android.view.KeyEvent
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.ArrayAdapter
import android.widget.ImageButton
import androidx.constraintlayout.widget.ConstraintLayout
import com.google.gson.JsonObject
import com.sexhack.sexharassment.adapter.NotifAdapter
import com.sexhack.sexharassment.model.CheckText
import com.sexhack.sexharassment.model.GetNotification
import com.sexhack.sexharassment.presenter.MainPresenter
import com.sexhack.sexharassment.presenter.MainRepository
import com.sexhack.sexharassment.view.GetNotifView
import com.sexhack.sexharassment.view.PostCheckView
import kotlinx.android.synthetic.main.keyboard_view.view.*
import org.jetbrains.anko.toast
import timber.log.Timber

class MyInputMethodService : InputMethodService(), KeyboardView.OnKeyboardActionListener,
    GetNotifView, PostCheckView {

    private lateinit var secondPresenter: MainPresenter<PostCheckView>
    private lateinit var presenter: MainPresenter<GetNotifView>
    private lateinit var keyboardView: KeyboardView
    private lateinit var keyboard: Keyboard
    private lateinit var clNotificationReport: ConstraintLayout
    private lateinit var btnBack: ImageButton
    private lateinit var spinnerAdapter: ArrayAdapter<String>
    private var berat = 0
    private var destinationId = "50"
    private var originId = "20"
    private var notificationList = mutableListOf<GetNotification>()
    private var root: View? = null
    private var caps = false
    var text = ""

    override fun onCreateInputView(): View {
        root = layoutInflater.inflate(R.layout.keyboard_view, null, false)
        secondPresenter = MainPresenter(this, MainRepository())
        presenter = MainPresenter(this, MainRepository())
        keyboardView = root?.keyboard_view!!
        clNotificationReport = root?.cl_notification_report!!
        btnBack = root?.btn_back!!

        btnBack.setOnClickListener {
            if (keyboardView.visibility == View.GONE) {
                keyboardView.visibility = View.VISIBLE
                btnBack.setImageResource(R.drawable.ic_report);
                clNotificationReport.visibility = View.GONE
            } else {
                keyboardView.visibility = View.GONE
                btnBack.setImageResource(R.drawable.ic_arrow_back);
                clNotificationReport.visibility = View.VISIBLE
                presenter.getNotif()
            }
        }

        keyboard = Keyboard(this, R.xml.keys_layout)
        keyboardView.keyboard = keyboard
        keyboardView.setOnKeyboardActionListener(this)
        return root as View
    }

    override fun onShowLoading() {
        root?.progress_keyboard?.visibility = View.VISIBLE
        text = ""
    }

    override fun onPostCheck(data: CheckText) {
        root?.progress_keyboard?.visibility = View.GONE
        val response = data.response
        Timber.e("pelecehan : $response")
        if (response == "Terdeteksi Pelecehan")
            toast("You can't say that")
    }

    override fun onPostCheckError(message: String) {
        root?.progress_keyboard?.visibility = View.GONE
        toast(message)
    }

    override fun onGetNotif(data: List<GetNotification>) {
        root?.progress_keyboard?.visibility = View.GONE
        val adapter = NotifAdapter()
        adapter.submitList(data)
        root?.rv_notification_report?.adapter = adapter
    }

    override fun onGetNotifError(message: String) {
        root?.progress_keyboard?.visibility = View.GONE

    }

    override fun onStartInputView(info: EditorInfo?, restarting: Boolean) {
        super.onStartInputView(info, restarting)
    }

    override fun swipeRight() {
    }

    override fun onPress(primaryCode: Int) {
    }

    override fun onRelease(primaryCode: Int) {
    }

    override fun swipeLeft() {
    }

    override fun swipeUp() {
    }

    override fun swipeDown() {
    }

    override fun onKey(primaryCode: Int, keyCodes: IntArray?) {
        val inputConnection = currentInputConnection
        if (inputConnection != null) {
            when (primaryCode) {
                Keyboard.KEYCODE_DELETE -> {
                    val selectedText = inputConnection.getSelectedText(0)

                    if (TextUtils.isEmpty(selectedText)) {
                        inputConnection.deleteSurroundingText(1, 0)
                    } else {
                        inputConnection.commitText("", 1)
                    }
                    keyboardView.invalidateAllKeys()
                }

                Keyboard.KEYCODE_SHIFT -> {
                    caps = !caps
                    keyboard.isShifted = caps
                    keyboardView.invalidateAllKeys()
                }

                Keyboard.KEYCODE_DONE -> {
                    val body = JsonObject()
                    body.addProperty("text", text)
                    secondPresenter.postCheck(body)
                    Timber.e("textnya : $text")
                    inputConnection.sendKeyEvent(
                        KeyEvent(
                            KeyEvent.ACTION_DOWN,
                            KeyEvent.KEYCODE_ENTER
                        )
                    )
                }
                else -> {
                    text += code(primaryCode)
                    Timber.e("textnya : $text")
                    inputConnection.commitText(code(primaryCode), 1)
                }
            }
        }
    }

    private fun code(primaryCode: Int): String {
        var code = primaryCode.toChar()
        if (Character.isLetter(code) && caps) {
            code = Character.toUpperCase(code)
        }
        return code.toString()
    }

    override fun onText(text: CharSequence?) {

    }
}