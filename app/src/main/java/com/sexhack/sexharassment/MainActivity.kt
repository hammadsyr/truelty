package com.sexhack.sexharassment

import android.app.Activity
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.view.View
import android.view.WindowManager
import android.widget.ProgressBar
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.widget.doAfterTextChanged
import com.sexhack.sexharassment.model.PostReport
import com.sexhack.sexharassment.presenter.MainPresenter
import com.sexhack.sexharassment.presenter.MainRepository
import com.sexhack.sexharassment.view.PostReportView
import kotlinx.android.synthetic.main.activity_main.*
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import org.jetbrains.anko.toast
import java.io.ByteArrayOutputStream
import java.io.File

class MainActivity : Activity(), PostReportView {
    private val REQUEST_IMAGE_CAPTURE = 1
    private val REQUEST_IMAGE_GALLERY = 2
    private var requestCapture = 0
    private var requestGallery = 0
    private val CAMERA_AND_STORAGE_PERMISSION = 3
    private var nReceiver: NotificationReceiver? = null
    private lateinit var presenter: MainPresenter<PostReportView>
    private lateinit var progress: ProgressBar
    private lateinit var messageFrom: MultipartBody.Part
    private var firstFile: MultipartBody.Part? = null
    private var secondFile: MultipartBody.Part? = null
    private var thirdFile: MultipartBody.Part? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        progress = progress_report
        presenter = MainPresenter(this, MainRepository())
        nReceiver = NotificationReceiver()
        val filter = IntentFilter()
        filter.addAction("com.sexhack.sexharassment.NOTIFICATION_LISTENER_EXAMPLE")
        registerReceiver(nReceiver, filter)

        messageFrom = MultipartBody.Part.createFormData("message_from", " ")
        btn_submit_report.setOnClickListener {
            presenter.postReport(messageFrom, firstFile, secondFile, thirdFile)
        }

        edt_from_report.doAfterTextChanged {
            messageFrom = MultipartBody.Part.createFormData("message_from", it.toString())
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        unregisterReceiver(nReceiver)
    }

    fun fileReport(v: View) {
        when (v.id) {
            R.id.btn_first_file_report -> {
                takePicture(R.id.btn_first_file_report)
            }
            R.id.btn_second_file_report -> {
                takePicture(R.id.btn_second_file_report)
            }
            R.id.btn_third_file_report -> {
                takePicture(R.id.btn_third_file_report)
            }
        }
    }

    private fun takePicture(id: Int) {
        //check storage and camera permission
        if (ContextCompat.checkSelfPermission(
                this,
                android.Manifest.permission.WRITE_EXTERNAL_STORAGE
            ) != PackageManager.PERMISSION_GRANTED ||
            ContextCompat.checkSelfPermission(
                this,
                android.Manifest.permission.CAMERA
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                this,
                arrayOf(
                    android.Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    android.Manifest.permission.CAMERA
                ),
                CAMERA_AND_STORAGE_PERMISSION
            )
        } else
            takePictureDialog(id)
    }

    private fun takePictureDialog(id: Int) {
        val builder = AlertDialog.Builder(this)
        builder.setMessage("Take a picture from...").setCancelable(true)
            .setPositiveButton("Camera") { _, _ ->
                Intent(MediaStore.ACTION_IMAGE_CAPTURE).also { takePictureIntent ->
                    takePictureIntent.resolveActivity(this.packageManager!!)?.also {
                        requestCapture = REQUEST_IMAGE_CAPTURE + id
                        startActivityForResult(
                            takePictureIntent, requestCapture
                        )
                    }
                }
            }.setNegativeButton("Gallery") { _, _ ->
                Intent(
                    Intent.ACTION_PICK,
                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI
                ).also { galleryIntent ->
                    galleryIntent.resolveActivity(this.packageManager!!)?.also {
                        requestGallery = REQUEST_IMAGE_GALLERY + id
                        startActivityForResult(galleryIntent, requestGallery)
                    }
                }
            }
        val alert = builder.create()
        alert.show()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == requestCapture && resultCode == RESULT_OK) {
            fromCamera(data)
        } else if (requestCode ==  requestGallery && resultCode == RESULT_OK) {
            fromGallery(data)
        }
    }

    private fun fromGallery(data: Intent?) {
        val tempUri = data?.data
        val finalFile = File(getRealPathFromURI(tempUri!!))
        val photo = BitmapFactory.decodeFile(finalFile.path)

        // create RequestBody instance from file
        val requestFile = RequestBody.create(
            MediaType.parse(
                "image/jpeg"
//                        context?.contentResolver?.getType(tempUri)
            ), finalFile
        )

        when {
            requestGallery - REQUEST_IMAGE_GALLERY == R.id.btn_first_file_report -> {
                btn_first_file_report.text = finalFile.name
                firstFile =
                    MultipartBody.Part.createFormData("screenshot_one", finalFile.name, requestFile)
            }
            requestGallery - REQUEST_IMAGE_GALLERY == R.id.btn_second_file_report -> {
                btn_second_file_report.text = finalFile.name
                secondFile =
                    MultipartBody.Part.createFormData("screenshot_two", finalFile.name, requestFile)
            }
            requestGallery - REQUEST_IMAGE_GALLERY == R.id.btn_third_file_report -> {
                btn_third_file_report.text = finalFile.name
                thirdFile =
                    MultipartBody.Part.createFormData(
                        "screenshot_three",
                        finalFile.name,
                        requestFile
                    )
            }
        }
    }

    private fun fromCamera(data: Intent?) {
        val photo = data?.extras?.get("data") as? Bitmap

        // CALL THIS METHOD TO GET THE URI FROM THE BITMAP
        val tempUri = getImageUri(photo!!)

        val finalFile = File(getRealPathFromURI(tempUri))

        // create RequestBody instance from file
        val requestFile = RequestBody.create(
            MediaType.parse(
                "image/jpeg"
            ), finalFile
        )

        when {
            requestCapture - REQUEST_IMAGE_CAPTURE == R.id.btn_first_file_report -> {
                btn_first_file_report.text = finalFile.name
                firstFile =
                    MultipartBody.Part.createFormData("screenshot_one", finalFile.name, requestFile)
            }
            requestCapture - REQUEST_IMAGE_CAPTURE == R.id.btn_second_file_report -> {
                btn_second_file_report.text = finalFile.name
                secondFile =
                    MultipartBody.Part.createFormData("screenshot_two", finalFile.name, requestFile)
            }
            requestCapture - REQUEST_IMAGE_CAPTURE == R.id.btn_third_file_report -> {
                btn_third_file_report.text = finalFile.name
                thirdFile =
                    MultipartBody.Part.createFormData(
                        "screenshot_three",
                        finalFile.name,
                        requestFile
                    )
            }
        }
    }

    private fun getImageUri(inImage: Bitmap): Uri {
        val bytes = ByteArrayOutputStream()
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes)

        val path =
            MediaStore.Images.Media.insertImage(this.contentResolver, inImage, "file", null)

        return Uri.parse(path)
    }

    private fun getRealPathFromURI(uri: Uri): String {
        var path = ""
        if (this.contentResolver != null) {
            val cursor = this.contentResolver?.query(uri, null, null, null, null)
            if (cursor != null) {
                cursor.moveToFirst()
                val idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA)
                path = cursor.getString(idx)
                cursor.close()
            }
        }
        return path
    }

//    fun buttonClicked(v: View) {
//        if (v.id == R.id.btnCreateNotify) {
//            val nManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
//            val ncomp: NotificationCompat.Builder = NotificationCompat.Builder(this)
//            ncomp.setContentTitle("My Notification")
//            ncomp.setContentText("Notification Listener Service Example")
//            ncomp.setTicker("Notification Listener Service Example")
//            ncomp.setSmallIcon(R.drawable.ic_launcher_background)
//            ncomp.setAutoCancel(true)
//            nManager.notify(System.currentTimeMillis().toInt(), ncomp.build())
//        } else if (v.id == R.id.btnClearNotify) {
//            val i = Intent("com.sexhack.sexharassment.NOTIFICATION_LISTENER_SERVICE_EXAMPLE")
//            i.putExtra("command", "clearall")
//            sendBroadcast(i)
//        } else if (v.id == R.id.btnListNotify) {
//            val i = Intent("com.sexhack.sexharassment.NOTIFICATION_LISTENER_SERVICE_EXAMPLE")
//            i.putExtra("command", "list")
//            sendBroadcast(i)
//        }
//    }

    internal inner class NotificationReceiver : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
//            val temp = intent.getStringExtra("notification_event") + "\n" + txtView!!.text
//            txtView!!.text = temp
        }
    }

    override fun onShowLoading() {
        progress.visibility = View.VISIBLE
        btn_submit_report.visibility = View.GONE
        window.setFlags(
            WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
            WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
    }

    override fun onPostReport(data: PostReport) {
        progress.visibility = View.GONE
        window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        btn_submit_report.visibility = View.VISIBLE
        Log.e("responnya : ", data.toString())
        toast("Successfully Reported")
    }

    override fun onPostReportError(message: String) {
        progress.visibility = View.GONE
        window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        btn_submit_report.visibility = View.VISIBLE
        toast(message)
    }
}