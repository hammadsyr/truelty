package com.sexhack.sexharassment.presenter

import com.google.gson.JsonObject
import com.sexhack.sexharassment.model.CheckText
import com.sexhack.sexharassment.model.GetNotification
import com.sexhack.sexharassment.model.PostNotification
import com.sexhack.sexharassment.model.PostReport
import com.sexhack.sexharassment.view.*
import okhttp3.MultipartBody

class MainPresenter<T>(private val view: T, private val repository: MainRepository) {

    fun postNotif(body: JsonObject) {
        if (view is PostNotifView) {
            view.onShowLoading()
            repository.postNotif(body, object :
                PostNotifCallback<PostNotification> {
                override fun onPostNotif(data: PostNotification) {
                    view.onPostNotif(data)
                }

                override fun onPostNotifError(message: String) {
                    view.onPostNotifError(message)
                }

            })
        }
    }

    fun getNotif() {
        if (view is GetNotifView) {
            view.onShowLoading()
            repository.getNotif(object :
                GetNotifCallback<List<GetNotification>> {
                override fun onGetNotifError(message: String) {
                    view.onGetNotifError(message)
                }

                override fun onGetNotif(data: List<GetNotification>) {
                    view.onGetNotif(data)
                }


            })
        }
    }

    fun postReport(
        messageFrom: MultipartBody.Part,
        firstFile: MultipartBody.Part?,
        secondFile: MultipartBody.Part?,
        thirdFile: MultipartBody.Part?
    ) {
        if (view is PostReportView) {
            view.onShowLoading()
            repository.postReport(messageFrom, firstFile, secondFile, thirdFile, object :
                PostReportCallback<PostReport> {
                override fun onPostReport(data: PostReport) {
                    view.onPostReport(data)
                }

                override fun onPostReportError(message: String) {
                    view.onPostReportError(message)
                }

            })
        }
    }

    fun postCheck(
        body: JsonObject
    ) {
        if (view is PostCheckView) {
            view.onShowLoading()
            repository.postCheck(body, object :
                PostCheckCallback<CheckText> {
                override fun onPostCheck(data: CheckText) {
                    view.onPostCheck(data)
                }

                override fun onPostCheckError(message: String) {
                    view.onPostCheckError(message)
                }

            })
        }
    }
}