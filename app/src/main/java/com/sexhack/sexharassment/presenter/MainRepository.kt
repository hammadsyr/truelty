package com.sexhack.sexharassment.presenter

import com.google.gson.JsonObject
import com.sexhack.sexharassment.model.CheckText
import com.sexhack.sexharassment.model.GetNotification
import com.sexhack.sexharassment.model.PostNotification
import com.sexhack.sexharassment.model.PostReport
import com.sexhack.sexharassment.util.RetrofitCl
import com.sexhack.sexharassment.util.Service
import com.sexhack.sexharassment.view.GetNotifCallback
import com.sexhack.sexharassment.view.PostCheckCallback
import com.sexhack.sexharassment.view.PostNotifCallback
import com.sexhack.sexharassment.view.PostReportCallback
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import okhttp3.MultipartBody
import retrofit2.Retrofit

class MainRepository {

    private lateinit var retrofit: Retrofit
    private lateinit var jsonApi: Service
    private lateinit var compositeDisposable: CompositeDisposable

    private fun setup() {
        retrofit = RetrofitCl.instance
        jsonApi = retrofit.create(Service::class.java)
        compositeDisposable = CompositeDisposable()
    }

    fun postNotif(
        body: JsonObject,
        callback: PostNotifCallback<PostNotification>
    ) {
        setup()
        compositeDisposable.add(
            jsonApi.postNotif(body)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ data ->
                    data.let {
                        callback.onPostNotif(it)
                    }
                }, {
                    callback.onPostNotifError(it.message!!)
                })
        )
    }

    fun getNotif(
        callback: GetNotifCallback<List<GetNotification>>
    ) {
        setup()
        compositeDisposable.add(
            jsonApi.getNotif()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ data ->
                    data.let {
                        callback.onGetNotif(it)
                    }
                }, {
                    callback.onGetNotifError(it.message!!)
                })
        )
    }

    fun postReport(
        messageFrom: MultipartBody.Part,
        firstFile: MultipartBody.Part?,
        secondFile: MultipartBody.Part?,
        thirdFile: MultipartBody.Part?,
        callback: PostReportCallback<PostReport>
    ) {
        setup()
        compositeDisposable.add(
            jsonApi.postReport(messageFrom, firstFile, secondFile, thirdFile)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ data ->
                    data.let {
                        callback.onPostReport(it)
                    }
                }, {
                    callback.onPostReportError(it.message!!)
                })
        )
    }

    fun postCheck(
        body: JsonObject,
        callback: PostCheckCallback<CheckText>
    ) {
        setup()
        compositeDisposable.add(
            jsonApi.postCheck(body)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ data ->
                    data.let {
                        callback.onPostCheck(it)
                    }
                }, {
                    callback.onPostCheckError(it.message!!)
                })
        )
    }
}