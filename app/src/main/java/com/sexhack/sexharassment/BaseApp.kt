package com.sexhack.sexharassment

import android.app.Application
import timber.log.Timber

class BaseApp : Application() {

    companion object {

        private lateinit var instance: BaseApp

        fun getInstance(): BaseApp {
            return instance
        }
    }

    override fun onCreate() {
        super.onCreate()

        instance = this
        if (BuildConfig.DEBUG)
            Timber.plant(Timber.DebugTree())

    }

}