package com.sexhack.sexharassment

import android.app.Notification
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.service.notification.NotificationListenerService
import android.service.notification.StatusBarNotification
import android.util.Log
import com.google.gson.JsonObject
import com.sexhack.sexharassment.model.PostNotification
import com.sexhack.sexharassment.presenter.MainPresenter
import com.sexhack.sexharassment.presenter.MainRepository
import com.sexhack.sexharassment.view.PostNotifView
import org.jetbrains.anko.toast

class NLService : NotificationListenerService(), PostNotifView {

    private val TAG = this.javaClass.simpleName
    private var nlservicereciver: NLServiceReceiver? = null
    private lateinit var presenter : MainPresenter<PostNotifView>

    override fun onCreate() {
        super.onCreate()
        presenter = MainPresenter(this, MainRepository())
        nlservicereciver = NLServiceReceiver()
        val filter = IntentFilter()
        filter.addAction("com.sexhack.sexharassment.NOTIFICATION_LISTENER_SERVICE_EXAMPLE")
        registerReceiver(nlservicereciver, filter)
    }

    override fun onDestroy() {
        super.onDestroy()
        unregisterReceiver(nlservicereciver)
    }

    override fun onNotificationPosted(sbn: StatusBarNotification) {
        Log.i(TAG, "**********  onNotificationPosted")
        Log.i(TAG, " ID :" + sbn.id + "\t" + sbn.notification.tickerText + "\t" + sbn.packageName)

        val body = JsonObject()
        body.addProperty("message",  sbn.notification.extras.getCharSequence(Notification.EXTRA_TEXT).toString())
        body.addProperty("from", sbn.notification.extras.getCharSequence(Notification.EXTRA_TITLE).toString())
        presenter.postNotif(body)

        val i = Intent("com.sexhack.sexharassment.NOTIFICATION_LISTENER_EXAMPLE")
        i.putExtra("notification_event", "onNotificationPosted :" + sbn.packageName + "\n")
        sendBroadcast(i)
    }

    override fun onNotificationRemoved(sbn: StatusBarNotification) {
        Log.i(TAG, "********** onNOtificationRemoved")
        Log.i(TAG, "ID :" + sbn.id + "\t" + sbn.notification.tickerText + "\t" + sbn.packageName)
        val i = Intent("com.sexhack.sexharassment.NOTIFICATION_LISTENER_EXAMPLE")
        i.putExtra("notification_event", "onNotificationRemoved :" + sbn.packageName + "\n")
        sendBroadcast(i)
    }

    internal inner class NLServiceReceiver : BroadcastReceiver() {
        override fun onReceive(
            context: Context, intent: Intent
        ) {
            if (intent.getStringExtra("command") == "clearall") {
                cancelAllNotifications()
            } else if (intent.getStringExtra("command") == "list") {
                val i1 = Intent("com.sexhack.sexharassment.NOTIFICATION_LISTENER_EXAMPLE")
                i1.putExtra("notification_event", "=====================")
                sendBroadcast(i1)
                var i = 1
                for (sbn in this@NLService.activeNotifications) {
                    val i2 = Intent("com.sexhack.sexharassment.NOTIFICATION_LISTENER_EXAMPLE")
                    i2.putExtra(
                        "notification_event",
                        i.toString() + " " + sbn.packageName + "\n"
                    )
                    sendBroadcast(i2)
                    i++
                }
                val i3 = Intent("com.sexhack.sexharassment.NOTIFICATION_LISTENER_EXAMPLE")
                i3.putExtra("notification_event", "===== Notification List ====")
                sendBroadcast(i3)
            }
        }
    }

    override fun onShowLoading() {}

    override fun onPostNotif(data: PostNotification) {
        Log.e("sukses ", data.response)
    }

    override fun onPostNotifError(message: String) {
        toast(message)
    }
}