package com.sexhack.sexharassment.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.PopupMenu
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.sexhack.sexharassment.R
import com.sexhack.sexharassment.databinding.ListNotificationReportBinding
import com.sexhack.sexharassment.model.GetNotification
import timber.log.Timber


class NotifAdapter :
    ListAdapter<GetNotification, NotifAdapter.ViewHolder>(
        NotifDiffCallback()
    ) {

    private lateinit var viewHolder: ViewHolder

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        viewHolder = ViewHolder(
            DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.list_notification_report, parent, false
            ), parent.context
        )
        return viewHolder
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    class ViewHolder(
        private val binding: ListNotificationReportBinding, private val context: Context
    ) : RecyclerView.ViewHolder(binding.root) {
        fun bind(getNotif: GetNotification) {

            binding.setClickListener { view ->
                val intent = Intent()
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                intent.setClassName(context, "com.sexhack.sexharassment.MainActivity")
                context.startActivity(intent)
//                context.startActivity<MainActivity>()
//                showPopup(view, context)
            }

            with(binding) {
                if(getNotif.is_harassment)
                    tvHarassmentNotifList.visibility = View.VISIBLE
                else
                    tvHarassmentNotifList.visibility = View.GONE
                setGetNotif(getNotif)
                Timber.e(getNotif.is_harassment.toString())
                executePendingBindings()
            }
        }

        private fun showPopup(view: View, context: Context) {
            val popup = PopupMenu(context, view)
            popup.inflate(R.menu.report_menu)

            popup.setOnMenuItemClickListener { item: MenuItem? ->

                when (item!!.itemId) {
                    R.id.report_menu -> {

                    }
                }
                true
            }
            popup.show()
        }
    }
}

private class NotifDiffCallback : DiffUtil.ItemCallback<GetNotification>() {
    override fun areItemsTheSame(oldItem: GetNotification, newItem: GetNotification): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: GetNotification, newItem: GetNotification): Boolean {
        return oldItem == newItem
    }
}